//Теоретический вопрос
//
// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
//
// 1) setTimeout() откладывает выполнение функции на указанный промежуток времени, в то время как setInterval() повторяет выполнение функции
//через указанный временной интервал.
//
// 2) Если в setTimeout() передать нулевую задержку, то выполнение функции отложится до того момента, пока не будет выполнен весь остальной код.
//
// 3) Вызов функции clearInterval() необходим для оптимизации работы системы, что бы не нагружать её ненужными процессами.


const image = document.querySelector('.image-to-show');
const startBtn = document.querySelector('.start');
const stopBtn = document.querySelector('.stop');
let counter = 1;

const changingImages = function () {
    $('.image-to-show').fadeOut(300);
    setTimeout(()=>{
        image.setAttribute('src', `./img/${counter}.jpg`);
    }, 300);
    $('.image-to-show').fadeIn(300);
    if (counter === 4) {
        counter = 1;
    } else {
        counter++;
    }
};

const startChanging = () => {
    changer = setInterval(changingImages, 9000);
};

const stopChanging = () => {
    clearInterval(changer);
};

const StartButtonChanger = () => {
    startBtn.classList.add('click');
    // startBtn.classList.remove('click')
    setTimeout(()=>startBtn.classList.remove('click'), 500)
};

const StopButtonChanger = () => {
    stopBtn.classList.add('click');
    // startBtn.classList.remove('click')
    setTimeout(()=>stopBtn.classList.remove('click'), 500)
};


const span = document.createElement("span");
let counterTimer = 9;
let counterTimer2 = 99;
span.innerText = `${counterTimer}.${counterTimer2}`;
document.body.prepend(span);

let timer = () => {
    span.innerText = `${counterTimer}.${counterTimer2}`;
    counterTimer--;
    if( counterTimer == -1) {
        counterTimer = 9;
    }
};
let timer2 = () => {
    span.innerText = `${counterTimer}.${counterTimer2}`;
    counterTimer2--;
    if( counterTimer2 == 10) {
        counterTimer2 = 99;
    }
};

startChanging();
let timerInterval =  setInterval(timer, 900);
let timerInterval2 = setInterval(timer2, 10);
stopTimer =() => {
    clearInterval(timerInterval);
    clearInterval(timerInterval2)
};

//
// let timerReload = () => {
//     span.innerText = `${counterTimer} ${counterTimer2}`;
//     counterTimer--;
//     if( counterTimer == -1) {
//         counterTimer = 10
//     }
// };





stopBtn.addEventListener('click', stopTimer);
startBtn.addEventListener('click', ()=>setInterval(timer, 900));
startBtn.addEventListener('click', ()=>setInterval(timer2, 10));
stopBtn.addEventListener('click', StopButtonChanger);
startBtn.addEventListener('click', StartButtonChanger);
startBtn.addEventListener('click', startChanging);
stopBtn.addEventListener('click', stopChanging);


// countDown() {
//     /**
//      * Calculate the timer
//      */
// это мы достаем из хранилища дату когда таймер должен закончиться и вычитаем из нее текущую дату
//     const timeLeft = this.timerFinish - (new Date().getTime() / 1000);
// если получилось отрицательное число, значит таймер закончился
//     if (timeLeft <= 0) {
//         this.exists = false;
//         return;
//     }
// посмотри документацию, что это значит, как можно работать с объектом Date
//     const timer = new Date();
//     timer.setTime(timeLeft * 1000);

//     const hours = timer.getUTCHours();
//     let minutes = timer.getUTCMinutes();
//     let seconds = timer.getUTCSeconds();
// здесь мы формируем строки для отображения - если меньше десити, значит выставляем 0 перед числом
//     minutes = minutes < 10 ? `0${minutes}` : minutes;
//     seconds = seconds < 10 ? `0${seconds}` : seconds;

//     if (hours > 0) {
//         this.timer = `${hours}:${minutes}:${seconds}`;
//     } else {
//         this.timer = `${minutes}:${seconds}`;
//     }
// }