

const buttonTheme = document.querySelector('.button-theme');

function showMenu () {
    const themesWrapper = document.createElement("div");
    themesWrapper.innerHTML =   '<button class="light">light</button>' +
                               '<button class="black">black</button>' +
                                '<span class="close">X</span>';
    themesWrapper.classList.add('theme-wrapper');
    document.body.prepend(themesWrapper);
    buttonTheme.style.opacity = 0.3;
    const closeButton = document.querySelector('.close');
    console.log(closeButton);
    closeButton.addEventListener('click', () => {themesWrapper.remove();buttonTheme.style.opacity = 1; } );
    const black = document.querySelector('.black');
    const clickBlack = () => {black.classList.remove('black');black.classList.add('clickBlack');};
    black.addEventListener('click', clickBlack);
    black.addEventListener('click', ()=>setTimeout(()=>black.classList.add('black2'), 500));
    black.addEventListener('click', ()=>setTimeout(()=>black.classList.remove('clickBlack'), 500));
    black.addEventListener('click', blackTheme);
    const light = document.querySelector('.light');
    const clickLight = () => {light.classList.remove('light');light.classList.add('clickLight');};
    light.addEventListener('click', clickLight);
    light.addEventListener('click', ()=>setTimeout(()=>light.classList.add('light2'), 500));
    light.addEventListener('click', ()=>setTimeout(()=>light.classList.remove('clickLight'), 500));
    light.addEventListener('click', lightTheme);

}

buttonTheme.addEventListener('click',showMenu );

const color = document.querySelector(':root');
console.log(color)

function blackTheme ()  {
    color.style.setProperty('--color1', '#a5a8a9');
    color.style.setProperty('--color2', '#256d1a');
    color.style.setProperty('--backGround', 'linear-gradient(rgba(0,0,0,0.5),\n' +
        '         rgba(0,0,0,0.7)),url("../image/rana-sawalha-364253-unsplash@1X.png")');
}
localStorage.setItem('themeColor', blackTheme);

function lightTheme () {
    color.style.setProperty('--color1', '#fff');
    color.style.setProperty('--color2', '#3cb878');
    color.style.setProperty('--backGround', 'url("../image/rana-sawalha-364253-unsplash@1X.png")');
    // localStorage.setItem('themeColor', themeLight)
}


document.addEventListener('DOMContentLoaded', ()=>{

        localStorage.removeItem('themeColor')




});
